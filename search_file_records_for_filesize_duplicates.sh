#!/usr/bin/env bash

### USAGE: search_file_records_for_filesize_duplicates.sh [string path_to_record_file] [int one_based_initial_record_n] [boolean stop_on_1st_dup_bool]
### 1. path_to_record_file : path to file to scan for duplicate filesizes
### 2. one_based_initial_record_n : 1st record/line number from which to scan
### 3. stop_on_1st_dup_bool : stop (or not) when 1st duplicate is found

### Defaults:
### * null(one_based_initial_record_n) -> one_based_initial_record_n=1
### * null(stop_on_1st_dup_bool) -> stop_on_1st_dup_bool=true
### * TODO: null(path_to_record_file) -> read from pipe/redirect

### Returns (details in usecase below || function=main):
### * (int) first_dupl_line_n : line# of 1st duplicate in file @ path_to_record_file
### * (int) dupls_count_int   : |duplicate records in current batch|
### * (string) dupls_str      : 2 or more records containing duplicates

### Line/record indices are 1-based (i.e., line#=1 for the top/1st line in a file), à la `emacs` and `sed`.

### Typical usecase:

### I have several backup sinks, which over time drift from equivalence: e.g., A will have a file not on B, or A and B will have the same file with different names or paths. This script is designed to assist with the 2nd problem, assuming that equality of filesize tends to imply equality of file contents. A typical/fastpath usecase for handling records with duplicate/equal filesize is:

### 1. Generate list of file records sorted on filesize. This script assumes this is done like

### 1.1. Generate list of file records (== lines) using my linux_misc_utils/list_files_by_path_size_crtime.sh (which, BTW, tab-delimits)

### 1.2. Sort that list on filesize using `sort --numeric-sort --key=2,2`, saving (read-only!) to filepath=path_to_record_file for use with this script. FWIW, `sort --numeric-sort --key=2,2 --reverse` works just as well for duplicate detection, and can be more useful for various tasks. This script assumes only that all file records with equal filesizes are adjacent.

### 2. Run `THIS path_to_record_file 1 true` , which returns as follows:

### 2.1. On error, script returns some integer < 0 (as single line).

### 2.2. If no dupls are detected (without error), script returns '0' (as a single line)

### 2.3. A "single duplicate" should never be returned, as this is considered nonsensical.

### 2.4. If 2 or more dupls are detected (without error), script returns one (given `stop_on_1st_dup_bool==true`--more below) delimiter-separated tuple
###      [first_dupl_line_n , dupls_count_int , dupls_str] where
###      dupls_str are the catenated duplicate records (currently as lines delimited by '\n')
###      dupls_count_int == |dupls_str| (i.e., their count)
###      first_dupl_line_n is the line# (in file @ path_to_record_file) where the 1st dupl record in dupls_str was found.

### If `stop_on_1st_dup_bool==false` && 2 or more *sets* of duplicates are detected, they will be returned as multiple tuples, each of the form given in item#=2.4 above.

### 3. Make changes to resolve duplicates. E.g.,

### 3.1. Calculate the value (( next_initial_record_n = dupls_count_int + first_dupl_line_n )) for subsequent searches (more below).

### 3.2. Copy dupls_str to an editor. Use that to

### 3.2.1. Script (e.g., with `bash` :-) changes to resolve duplicates (e.g., delete or rename files) in current backup sink.

### 3.2.2. Save above scriptlet (to some file) for use when making changes to other backup sinks (for identical reproduction), if you have them.

### 4. For subsequent searches, run `THIS path_to_record_file ${next_initial_record_n} false` in order to begin search at the file position *after* the most-recent-previous batch of duplicates.. This will return either

### 4.1. if more dupls exist: the next batch of duplicates (should they exist) as detailed in item#=2.4 above. Handle those with procedure like item#=3 above, then loop until ...

### 4.2. if more dupls !exist: this script returns like item#=2.2 above, and this usecase terminates.

### Following attempts to observe Google Shell Style Guide currently @ https://google.github.io/styleguide/shell.xml

### Dependencies: see functions below.

### TODO:
### * incorporate TODOs from code below (e.g., pipe/redirect)
### * resolve how to `declare` boolean (i.e., how to do it, or that it's unsupported by current `bash` versions)
### * create/run testcases from examples
### * document dependencies in program header (not just function headers).
### * (always) run `shellcheck` pre-commit!

### Copyright (C) 2019 Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
### Latest version of this file (or pointer to same) should be available @
### http://bitbucket.org/tlroche/backup_utils/src/HEAD/search_file_records_for_filesize_duplicates.sh

### ------------------------------------------------------------------
### directives
### ------------------------------------------------------------------

## `Case` statement faaar below (function=process_search_results) requires extended pattern matching operators
## Gotta turn that on before *function definition* per https://stackoverflow.com/a/34913651/915044
## TODO: determine where to turn this on/off with least effect on environment
shopt -s extglob

### ------------------------------------------------------------------
### constants
### ------------------------------------------------------------------

## Messaging constants

declare -r THIS_FP="${BASH_SOURCE}" # works when either executing or `source`ing
declare -r THIS_FN="$(basename "${THIS_FP}")"
declare -r THIS_DIR="$(readlink -f "$(dirname "${THIS_FP}")")"  # FQ path to caller's $(pwd)
declare -r MESSAGE_PREFIX="${THIS_FN}:"
declare -r DEBUG_PREFIX="${MESSAGE_PREFIX} DEBUG:"
declare -r ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
declare -r VERSION_N='0.1'
declare -r VERSION_STR="${THIS_FN} version=${VERSION_N}"

## Default values

# Currently, THIS_FP requires a readable path_to_record_file , so no default (see TODO)
# declare -r path_to_record_file_DEF=''
# By default, search entire file (from line#=1 to end)
declare -ri one_based_initial_record_n_DEF=1
# By default, return only 1 set of dupls at a time.
declare -r stop_on_1st_dup_bool_DEF=true # TODO: how to `declare` boolean?
# TODO: support configuration of (internal) delimiter without touching this file!
# TODO? support (internal) delimiters with length > 1 ???
declare -r TUPLE_INTER_DELIM_CHAR='|'

### -----------------------------------------------------------------------
### code
### -----------------------------------------------------------------------

### Nothing here: goto section=payload || section=functions

### -----------------------------------------------------------------------
### functions
### -----------------------------------------------------------------------

### Process file possibly containing duplicates. KEEP THIS SIMPLE!
### Here mostly just handle argument passing and delegation (and debugging)

### Arguments:
### 1. path_to_record_file : path to file to scan for duplicate filesizes
### 2. one_based_initial_record_n : 1st record/line number from which to scan
### 3. stop_on_1st_dup_bool : stop (or not) when 1st duplicate is found

### Defaults: see global section=constants.

### Returns: (TODO: keep this sync-ed with program header, or eliminate duplication?)

### 1. On error: some integer < 0 (as single line).

### 2. If no dupls are detected (without error): '0' (as a single line)

### 3. If 2 or more dupls are detected (without error) &&

### 3.1. (stop_on_1st_dup_bool==true): one delimiter-separated tuple
###      [first_dupl_line_n , dupls_count_int , dupls_str] where
###      dupls_str are the catenated duplicate records (currently as lines delimited by '\n')
###      dupls_count_int == |dupls_str| (currently, their line count)
###      first_dupl_line_n is the line# (in file @ path_to_record_file) where the 1st dupl record in dupls_str was found.

### 3.2. (stop_on_1st_dup_bool==false): one or more tuples separated by '\n'.
###      I.e., tuples are *internally* separated with TUPLE_INTER_DELIM_CHAR but *externally* separated with '\n'.

### Delegates:
### * function search_file: does the actual file searching, returns raw contents for ...
### * function process_search_results: converts raw search results into suitable THIS->retval

### TODO: enhance to handle pipe/redirect.
function main() {
    ## note local copies of readonly globals are forbidden in some bash versions
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_DEBUG_PREFIX="${FN_MESSAGE_PREFIX} DEBUG:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"

    ## raw args
    local -r path_to_record_file_ARG="${1}"
    local -ri one_based_initial_record_n_ARG="${2}"
    local -r stop_on_1st_dup_bool_ARG="${3}" # TODO: how to `declare` boolean?

    ## internal and return values
    local path_to_record_file
    local -i one_based_initial_record_n
    local stop_on_1st_dup_bool # TODO: how to `declare` boolean?
    local raw_result_str=''    # raw string from searching source file
    local result_str=''        # string (possibly) containing duplicates (but no decorations)
    local -i results_n=0       # |result_str| in lines
    local ret_val=''

#     ## debugging raw args
#     >&2 echo "${FN_DEBUG_PREFIX} path_to_record_file_ARG='${path_to_record_file_ARG}'"
#     >&2 echo "${FN_DEBUG_PREFIX} one_based_initial_record_n_ARG='${one_based_initial_record_n_ARG}'"
#     >&2 echo "${FN_DEBUG_PREFIX} stop_on_1st_dup_bool_ARG='${stop_on_1st_dup_bool_ARG}'"
#     ## end debugging

    ## assign internals based on args or defaults
    if   [[ -z "${path_to_record_file_ARG}" ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} missing path_to_record_file_ARG, exiting ..."
        exit 3
    elif [[ -r "${path_to_record_file_ARG}" ]] ; then
        path_to_record_file="${path_to_record_file_ARG}" # TODO: make FQ
    else
        >&2 echo "${FN_ERROR_PREFIX} cannot read record file path='${path_to_record_file_ARG}', exiting ..."
        exit 4
    fi

    if   [[ -z "${one_based_initial_record_ARG}" ]] ; then
        (( one_based_initial_record_n=one_based_initial_record_n_DEF ))
    elif (( one_based_initial_record_n_ARG > 0 )) ; then
        (( one_based_initial_record_n=one_based_initial_record_n_ARG ))
    else
        >&2 echo "${FN_ERROR_PREFIX} cannot handle one_based_initial_record_n_ARG='${one_based_initial_record_n_ARG}', exiting ..."
        exit 5
    fi

    # TODO: how to do combination ('&&' , '||') tests of "real bash booleans"?
    # note "real bash booleans" have "naked" tests
    if   [[ -z "${stop_on_1st_dup_bool_ARG}" ]] ; then
        stop_on_1st_dup_bool=${stop_on_1st_dup_bool_DEF}
    elif ${stop_on_1st_dup_bool_ARG} ; then
        stop_on_1st_dup_bool=true
    elif ! ${stop_on_1st_dup_bool_ARG} ; then
        stop_on_1st_dup_bool=false
    else
        >&2 echo "${FN_ERROR_PREFIX} stop_on_1st_dup_bool !in (-z , true , false) ??? exiting ..."
        exit 6
    fi

#     >&2 echo "${FN_DEBUG_PREFIX} about to search with"
#     >&2 echo "${FN_DEBUG_PREFIX} path_to_record_file='${path_to_record_file}'"
#     >&2 echo "${FN_DEBUG_PREFIX} one_based_initial_record_n='${one_based_initial_record_n}'"
#     >&2 echo "${FN_DEBUG_PREFIX} stop_on_1st_dup_bool='${stop_on_1st_dup_bool}'"

    # Don't quote: retain line structure
    raw_result_str=$(search_file "${path_to_record_file}" ${one_based_initial_record_n} ${stop_on_1st_dup_bool})
    ret_val="${?}"

    >&2 echo "${FN_DEBUG_PREFIX} search_file returns with"
    >&2 echo "${FN_DEBUG_PREFIX} raw_result_str='${raw_result_str}'"

    if   (( ret_val != 0 )) ; then
        >&2 echo "${FN_ERROR_PREFIX} ret_val='${ret_val}', exiting ..."
        exit 7
    elif [[ -z "${raw_result_str}" ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} null raw_result_str, exiting ..."
        exit 8
    else
        # don't quote, keep line structure (if present)
        result_str=$(process_search_results ${stop_on_1st_dup_bool} ${raw_result_str})
        ret_val="${?}"
        >&2 echo "${FN_DEBUG_PREFIX} process_search_results returns '${result_str}'"
        if   (( ret_val != 0 )) ; then
            >&2 echo "${FN_ERROR_PREFIX} ret_val='${ret_val}', exiting ..."
            exit 9
        fi

        ## Process result_str.
        if   [[ -z "${result_str}" ]] ; then
            >&2 echo "${FN_ERROR_PREFIX} null result_tuples, exiting"
            exit 10
        elif (( result_str == 0 )) ; then
            >&2 echo "${FN_DEBUG_PREFIX} found 0 duplicates, exiting"
            exit 0
        ## if (is_int(result_str) && (result_str < 0)):
        ##     there's been an error, and process_search_results should have previously exited.
        ## if (is_int(result_str) && (result_str > 0)):
        ##     something is very wrong, and we also shouldn't be here
        fi

        ## else: we should have real tuples to process

### Returns: (TODO: keep this sync-ed with program header, or eliminate duplication?)

### 3. If 2 or more dupls are detected (without error) &&

### 3.1. (stop_on_1st_dup_bool==true): one delimiter-separated tuple
###      [first_dupl_line_n , dupls_count_int , dupls_str] where
###      dupls_str are the catenated duplicate records (currently as lines delimited by '\n')
###      dupls_count_int == |dupls_str| (currently, their line count)
###      first_dupl_line_n is the line# (in file @ path_to_record_file) where the 1st dupl record in dupls_str was found.

### 3.2. (stop_on_1st_dup_bool==false): one or more tuples separated by '\n'.
###      I.e., tuples are *internally* separated with TUPLE_INTER_DELIM_CHAR but *externally* separated with '\n'.

        # return it: don't quote, retain line structure (I hope)
        echo ${result_tuples}
    fi # end if   (( ret_val != 0 ))
#    exit 0 # hoses `source`ing
    return 0
} # end function main

### Actually search the source file, return raw results for processing.

### Arguments:
### 1. path_to_record_file : path to file to scan for duplicate filesizes
### 2. one_based_initial_record_n : 1st record/line number from which to scan
### 3. stop_on_1st_dup_bool : stop (or not) when 1st duplicate is found
### ASSERT: validity of ∀ args ensured by caller!
### Defaults: NONE.

### Returns:
### 1. On error: some integer < 0 (as single line), probably (0 - my `exit` code)
### 2. If no dupls are detected (without error): '0' (as a single line)
### 3. If 2 or more dupls are detected (without error), return raw results to caller for processing.

### Dependencies:
### * `awk`
### * `tail`, but TODO: improve `awk` program in this function to do that work internally
function search_file() {
    ## note local copies of readonly globals are forbidden in some bash versions
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_DEBUG_PREFIX="${FN_MESSAGE_PREFIX} DEBUG:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"

    ## raw args ... but since validity ensured by caller, just assign to internals
    local -r path_to_record_file="${1}"
    local -ri one_based_initial_record_n="${2}"
    local -r stop_on_1st_dup_bool="${3}" # TODO: how to `declare` boolean?

    ## internal and return values, excepting ...
    local result_str=''                     # string (possibly) containing all returned info
    local current_set_result_str=''         # string (possibly) containing duplicates (but no decorations) for current dupl set
    local -i current_set_results_n=0        # |current dupl set| in lines
    local -i current_set_1st_dupl_line_n=0  # line# (in source) for 1st dupl in current set
    local ret_val=''                  # aka exit status aka errorcode aka errval, not data

    ## ... `awk` internals
    local awk_result_str=''           # string (possibly) very `awk`ward results

    # Awk-isms: expanded from http://stackoverflow.com/a/53206688/915044 and https://unix.stackexchange.com/a/198067/38638
    # OFS == output field separator
    # NR  == |total records| (~= all lines compared ?)
    # Notes:
    # * `awk` seems *very* sensitive to whitespace!
    # * TODO: discover how to do the `tail`ing (below) with just `awk` (i.e., in this string).
    #   (FWIW, I tried adapting https://superuser.com/a/904997/98270 but failed.)
    # * DON'T s/\/\\/ in awk_program_str ! (except in comments):
    #   it will make `echo -e "${awk_program_str}"` look pretty, but it will put *literal* \t and \n in your output!
    local -r awk_program_str='
    BEGIN {
        OFS="\t"         # tab-delimit output fields. DO NOT double-backslash!
        ORS="\n"         # output records as lines.   DO NOT double-backslash!
    }
    ($2 in a) {          # save duplicates in 2nd field to array=a
        if(a[$2]) {      # if found
            print a[$2]  # output 1st/stored dupl, and ...
            a[$2]=""     # ... mark it as output-ed
        }
        print NR,$0      # print the duplicate
        next             # repeat for next dupl
    }{                   # generate output (NOT an 'END' block!)
        a[$2]=NR OFS $0  # prefix output with line#=NR (then \\t,) then full record
    }'

    ## Gotta put debugging onto stderr, else it will [accumulate,return] to function=main!
#     >&2 echo "${FN_DEBUG_PREFIX} raw args:"
#     >&2 echo "${FN_DEBUG_PREFIX} path_to_record_file='${path_to_record_file}'"
#     >&2 echo "${FN_DEBUG_PREFIX} one_based_initial_record_n='${one_based_initial_record_n}'"
#     >&2 echo "${FN_DEBUG_PREFIX} stop_on_1st_dup_bool='${stop_on_1st_dup_bool}'"

    ## search with `awk`
#    >&2 echo "${FN_DEBUG_PREFIX} about to run 'awk' with awk_program_str='${awk_program_str}'"

    if (( one_based_initial_record_n > 1 )) ; then
        # DON'T quote awk_result_str : preserve line structure
        awk_result_str=$(tail --lines=+${one_based_initial_record_n} "${path_to_record_file}" | awk "${awk_program_str}")
    else
        awk_result_str=$(awk "${awk_program_str}" "${path_to_record_file}")
    fi
    ret_val="${?}"

#    >&2 echo "${FN_DEBUG_PREFIX} after running 'awk'"
#    >&2 echo "${FN_DEBUG_PREFIX} awk_result_str='${awk_result_str}'"

    if   (( ret_val != 0 )) ; then
        >&2 echo "${FN_ERROR_PREFIX} FAILED: ret_val='${ret_val}', exiting ..."
        echo '-9' # "returning" errorcode
        exit 11
    elif [[ -z "${awk_result_str}" ]] ; then
        echo '0'  # return current_set_results_n=0 on stdout, and
        return 0
    fi

    >&2 echo "${FN_DEBUG_PREFIX} about to return with:"
    >&2 echo "${FN_DEBUG_PREFIX} awk_result_str='${awk_result_str}'"

#    exit 0 # hoses `source`ing
    return 0
} # end function search_file

### Process search results into form suitable for returning from function=main.

### Arguments:
### 1. stop_on_1st_dup_bool : (from main) stop (or not) when 1st duplicate is found
###    Implies that this should return |duplicate sets| ∈ {0,1}
###    TODO: (stop_on_1st_dup_bool &&& (|duplicate sets| > 1)) ? we need to upgrade search_file::awk_program
### 2. raw_result_str : search results (from function=search_file)

### Defaults: none. ASSERT: callers are responsible for validity of arguments.

### Returns: (TODO: keep this sync-ed with {program header, function=main}, or eliminate duplication?)

### 1. On error: some integer < 0 (as single line).

### 2. If no dupls are detected (without error): '0' (as a single line)

### 3. If 2 or more dupls are detected (without error) &&

### 3.1. (stop_on_1st_dup_bool==true): one delimiter-separated tuple
###      [first_dupl_line_n , dupls_count_int , dupls_str] where
###      dupls_str are the catenated duplicate records (currently as lines delimited by '\n')
###      dupls_count_int == |dupls_str| (currently, their line count)
###      first_dupl_line_n is the line# (in file @ path_to_record_file) where the 1st dupl record in dupls_str was found.

### 3.2. (stop_on_1st_dup_bool==false): one or more tuples separated by '\n'.
###      I.e., tuples are *internally* separated with TUPLE_INTER_DELIM_CHAR but *externally* separated with '\n'.
function process_search_results() {
    ## note local copies of readonly globals are forbidden in some bash versions
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_DEBUG_PREFIX="${FN_MESSAGE_PREFIX} DEBUG:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"

    ## raw args ... but since validity ensured by caller, just assign to internals
    local -r stop_on_1st_dup_bool="${1}" # TODO: how to `declare` boolean?
    # this is (potentially) a long and delimited string, so don't take $2
    shift 1                      # remove $1, then ...
#    local -r raw_record_str=${@} # ... take rest of args # works but annoys `shellcheck`
    local -r raw_record_str=${*} # ... take rest of args

    >&2 echo "${FN_DEBUG_PREFIX} raw args:"
    >&2 echo "${FN_DEBUG_PREFIX} stop_on_1st_dup_bool='${stop_on_1st_dup_bool}'"
    >&2 echo "${FN_DEBUG_PREFIX} raw_record_str='${raw_record_str}'"

    ## internal and return values
    local result_str=''                     # string (possibly) containing all returned info
    local current_set_result_str=''         # string (possibly) containing duplicates (but no decorations) for current dupl set
    local -i current_set_results_n=0        # |current dupl set| in lines
    local -i current_set_1st_dupl_line_n=0  # line# (in source) for 1st dupl in current set
    local ret_val=''                    # aka exit status aka errorcode aka errval, not data

    ## start testing raw input
    >&2 echo "${FN_DEBUG_PREFIX} attempting to process integer values of raw_record_str"
    ## For *much* more about bash `case` statements, including
    ## * their match *patterns* (which are NOT regexps! if ya wanna use those, gotta if/then) see also
    ##   https://www.gnu.org/software/bash/manual/html_node/Pattern-Matching.html
    ## * their clause terminators ‘;;’ ‘;&’ ‘;;&’ see also
    ##   https://www.gnu.org/software/bash/manual/html_node/Conditional-Constructs.html
    ## see https://unix.stackexchange.com/a/525551/38638
    case ${raw_record_str} in # don't quote, in case it's multiline
        '')
            # empty string, no need for `if [[ -z ...`
            >&2 echo "${FN_ERROR_PREFIX} null raw_result_str, exiting ..."
            exit 12
            ;;
        [+-]0)
            >&2 echo "${FN_ERROR_PREFIX} zero should not be signed, exiting ..."
            exit 13
            ;;
        # Note: start getting syntax errors here and below ? gotta `shopt -s extglob`
        ?(-|+)+([[:digit:]]))
            # it's an integer, so just say so and fallthrough
            >&2 echo "${FN_DEBUG_PREFIX} raw_record_str is an integer"
#            ;& # this only runs the next clause, thanks frostschutz
            ;;& # works
        -+([[:digit:]]))
            # it's negative: there's been an error somewhere else, so just return it and `return`
            >&2 echo "${FN_DEBUG_PREFIX} raw_record_str is negative integer"
            echo "${raw_record_str}"
            return "${raw_record_str}"
            ;;
        0)
            # it's zero: function=search_file found nothing
            >&2 echo "${FN_DEBUG_PREFIX} raw_record_str==0"
            echo '0'    # just return it, and ...
            return 0
            ;;
        ?(+)+([[:digit:]]))
            # it's just a positive integer, nothing else? there's been an error, so ...
            >&2 echo "${FN_DEBUG_PREFIX} raw_record_str is positive integer?"
            echo "${raw_record_str}"
            return "${raw_record_str}"
            ;;
        *)
            # it's not (just) an integer, so do nothing and exit case (handle later)
            >&2 echo "${FN_DEBUG_PREFIX} raw_record_str !integer"
            ;;
    esac

# ----------------------------------------------------------------------

    return 0 # TODO!

    ## count and process |duplicate records|
    ## PROBLEM: what if `awk_result_str` contains *multiple* dupl sets?
    ## TODO: (ideally) make `awk_program_str` respect `stop_on_1st_dup_bool` (else) brute-force detection on field#=2
    current_set_results_n=$(( $(echo "${awk_result_str}" | wc -l) ))
    # `cut --fields='2-'` -> return all fields *except* 1st, which is the `awk`-prepended (see below)
    # don't quote: retain line structure
    current_set_result_str=$(echo "${awk_result_str}" | cut --fields='2-')

    ## `awk` program (above) prepends record# of each duplicate record was prepended (see `NR` above) to it ...
    current_set_1st_dupl_line_n=$(( $(echo "${awk_result_str}" | head --lines 1 | cut --fields=1) ))
    ## ... but that's relative to one_based_initial_record_n ! so
    if   (( one_based_initial_record_n == 1 )) ; then
        (( current_set_1st_dupl_line_n=one_based_initial_record_n ))
    elif (( one_based_initial_record_n > 1 )) ; then
        (( current_set_1st_dupl_line_n = ( one_based_initial_record_n - 1 ) ))
    else
        ## TODO: caller failure!
        >&2 echo "${FN_ERROR_PREFIX} one_based_initial_record_n='${one_based_initial_record_n}' < 1, exiting ..."
        echo '-10' # "returning" errorcode
        exit 14
    fi

## If we're here, we've gotta [assemble,return] result_str ...
## which !stop_on_1st_dup_bool -> contains multiple dupl sets!

### 3.1. (stop_on_1st_dup_bool==true): one delimiter-separated tuple
###      [current_set_1st_dupl_line_n , current_set_results_n , dupls_str] where
###      dupls_str are the catenated duplicate records (currently as lines delimited by '\n')
###      current_set_results_n == |dupls_str| (currently, their line count)
###      current_set_1st_dupl_line_n is the line# (in file @ path_to_record_file) where the 1st dupl record in dupls_str was found.

### 3.2. (stop_on_1st_dup_bool==false): one or more tuples separated by '\n'.
###      I.e., tuples are *internally* separated with TUPLE_INTER_DELIM_CHAR but *externally* separated with '\n'.

    ## process args=${stop_on_1st_dup_bool} , ${result_str}
    results_n=$(echo "${result_str}" | wc -l)
    #    if   (( results_n <= 0 )) -> -z (tested in main)
    if   (( results_n == 1 )) ; then
        ## valid result_str for no dupls found
        if   (( result_str == 0 )) ; then
            # return it
            echo "${result_str}"
            exit 0
        else
            >&2 echo "${FN_ERROR_PREFIX} result_str='${result_str}', exiting ..."
            exit 15
        fi

    fi
} # end function process_search_results

### ------------------------------------------------------------------
### `source` fence
### ------------------------------------------------------------------

### I.e., don't run any following lines (to EOF) if this file is `source`d

if [[ "${BASH_SOURCE}" != "${0}" ]] ; then
    return 0
fi

### -----------------------------------------------------------------------
### payload
### -----------------------------------------------------------------------

### Functionality code goes here. KEEP THIS SIMPLE!
### Call function=main (if there are any functions) per Shell Style Guide.
### TODO: read input data from pipe/redirect (as alternative to passing filepath to data)

if  (( ${#} == 0 )) ; then # zero commandline arguments à la pipe/redirect
    >&2 echo "${ERROR_PREFIX} cannot currently handle |commandline arguments| == 0, exiting ..."
    exit 11
else
#    >&2 echo "${DEBUG_PREFIX} |commandline arguments| == ${#}"
    main "${@}"
# following makes `shellcheck` complain:
# > SC2048: Use "$@" (with quotes) to prevent whitespace problems.
#    main ${*}
fi

### ------------------------------------------------------------------
### execution fence
### ------------------------------------------------------------------

## I.e., don't run any following lines (to EOF), period.

if [[ "${BASH_SOURCE}" == "${0}" ]] ; then
    # ... if this file is executed
    exit 0
else
    # ... if this file is `source`d
    return 0
fi

### ------------------------------------------------------------------
### testcases
### ------------------------------------------------------------------

## Discuss

## * how to test

## * when/where last tested

## -------------------------------------------------------------------
## executable
## -------------------------------------------------------------------

## goto ./%%%__tests.sh

## -------------------------------------------------------------------
## examples
## -------------------------------------------------------------------

## -------------------------------------------------------------------
## example 0
## -------------------------------------------------------------------

declare -ri EXAMPLE_N=0

### Test with dupl-free fixture data, mode={commandline, normal, not `source`}.
### Run from THIS_DIR.
### Cut'n'paste any following stanza(s) into console after setting following constant:
declare -r THIS_FP='./search_file_records_for_filesize_duplicates.sh' # set to (preferably FQ) path to this executable

declare -r MESSAGE_PREFIX="$(basename "${THIS_FP}")::example ${EXAMPLE_N}:"
declare -r DEBUG_PREFIX="${MESSAGE_PREFIX} DEBUG:"
declare -r ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

declare path_to_record_file='INIT'        # filepath (and file contents) initialized in fixture
declare -i one_based_initial_record_n=1   # line# in source from which to begin search
declare stop_on_1st_dup_bool=true         # false -> continue to search for additional dupls

## NOTE:
## * Don't initialize following to same values as defaults! Gotta test that they get changed.

declare ret_val='-1'                      # aka exit status aka errorcode aka errval, NOT ...
declare fixture0_results_str='INIT'       # ... raw data returned via stdout, containing:
declare -i fixture0_dupls_count_int=-1    # count (>= 0) of returned dupls
declare -i fixture0_first_dupl_line_n=-1  # line# (>= 1) of 1st returned dupl in source file
declare fixture0_dupls_str='INIT'         # (only) dupls found in this search (if any)

### Fixture 0: mock data with no duplicate records in numeric field index=2 (TODO: modularize)
path_to_record_file="$(mktemp)"
echo -e "foo\t100\t$(date)"       > "${path_to_record_file}"
echo -e "foo\t90\t$(date)"       >> "${path_to_record_file}"
echo -e "foo\t80\t$(date)"       >> "${path_to_record_file}"
echo -e "foo\t70\t$(date)"       >> "${path_to_record_file}"
echo -e "foo\t0\t$(date)"        >> "${path_to_record_file}"
echo -e "foo\t-1000\t$(date)"    >> "${path_to_record_file}"
echo "${MESSAGE_PREFIX} source file listing:"
ls -al "${path_to_record_file}"
echo "${MESSAGE_PREFIX} source file contents (fake line#s appended by 'nl'):"
nl "${path_to_record_file}"

### Test 0: should {find no duplicates, not abend (retval=0)}

echo "${DEBUG_PREFIX} about to run:"
echo "${THIS_FP} ${path_to_record_file} ${one_based_initial_record_n} ${stop_on_1st_dup_bool}"
fixture0_results_str=$("${THIS_FP}" "${path_to_record_file}" ${one_based_initial_record_n} ${stop_on_1st_dup_bool})
ret_val="${?}"
echo "${DEBUG_PREFIX} ret_val='${ret_val}'"

if (( ret_val == 0 )) ; then
    if (( fixture0_results_str == 0 )) ; then
        echo "${MESSAGE_PREFIX} PASSED: no duplicates found"
        echo "complete input:"
        nl "${path_to_record_file}"
    else
        >&2 echo "${ERROR_PREFIX} FAILED: fixture0_dupls_count_int='${fixture0_dupls_count_int}' && fixture0_first_dupl_line_n='${fixture0_first_dupl_line_n}'"
        >&2 echo 'fixture0_dupls_str='
        >&2 echo "${fixture0_dupls_str}"
        >&2 echo "complete input:"
        >&2 nl "${path_to_record_file}"
        >&2 echo "${error_prefix} exiting ..."
        exit 16
    fi
else # (( ret_val != 0 )) ; then
    >&2 echo "${ERROR_PREFIX} FAILED: ret_val='${ret_val}', exiting ..."
    exit 17
fi
